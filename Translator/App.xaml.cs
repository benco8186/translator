﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using Translator.Service;
using Translator.Viewmodel;

namespace Translator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string ERROR_NOT_HANDLED = "Erreur non traité : ";
        public IConfiguration Configuration { get; private set; }
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show($"{ERROR_NOT_HANDLED} {e.Exception.Message}");
        }

        private readonly IHost host;

        public static IServiceProvider ServiceProvider { get; private set; }
        //[STAThread]
        //public static void Main()
        //{
        //    var application = new App();
        //    application.InitializeComponent();
        //    application.Run();
        //}
        public App()
        {
            // Original host configuration...
            host = new HostBuilder().ConfigureServices((context, service) => ConfigureServices(service)).Build();
            ServiceProvider = host.Services;
        }

        private void ConfigureServices(
            IServiceCollection services)
        {
            //services.Configure<AppSettings>(configuration
            //    .GetSection(nameof(AppSettings)));
            services.AddScoped<ITranslateService, TranslateService>();

            // Register all ViewModels.
            services.AddSingleton<ApplicationViewmodel>();

            // Register all the Windows of the applications.
            //services.AddTransient<MainWindow>();
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            Configuration = builder.Build();
            await host.StartAsync();

            //var window = ServiceProvider.GetRequiredService<MainWindow>();
            //window.Show();

            base.OnStartup(e);
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            host.Dispose();

        }
    }


}
