﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Translator.Tool;

namespace Translator.Command
{
    /// <summary>
    /// <c>IAsyncCommand</c>
    /// Définit le contrat pour les classes de commandes asynchrones
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="App_Fusion_Excel.Tool.ICommandWithEvent{System.String}" />
    public interface IAsyncCommand : ICommandWithEvent
    {
        Task ExecuteAsync();
        bool CanExecute();
    }

    /// <summary>
    /// <c>AsyncCommand</c>
    /// Classe permettant d'effectuer des commandes asynchrones 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="App_Fusion_Excel.Tool.IAsyncCommand{T}" />
    public class AsyncCommand : IAsyncCommand
    {
        #region Fields
        private bool _isExecuting;
        private readonly Func<Task> _execute = null;
        private readonly Func<bool> _canExecute = null;
        #endregion Fields

        #region Events
        public event Action WorkStart;
        public event Action WorkEnd;
        public event Action<string> WorkError;
        public event EventHandler CanExecuteChanged;
        #endregion Events


        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncCommand{T}"/> class.
        /// </summary>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        /// <param name="errorHandler">The error handler.</param>
        public AsyncCommand(
            Func<Task> execute,
            Func<bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        /// <summary>
        /// Determines whether this instance can execute.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance can execute; otherwise, <c>false</c>.
        /// </returns>
        public bool CanExecute()
        {
            return !_isExecuting && (_canExecute?.Invoke() ?? true);
        }

        public async Task ExecuteAsync()
        {
            if (CanExecute())
            {
                try
                {
                    _isExecuting = true;
                    await _execute();
                    WorkEnd?.Invoke();
                }
                finally
                {
                    _isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Raises the can execute changed.
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Explicit implementations        
        /// <summary>
        /// Définit la méthode qui détermine si la commande peut s'exécuter dans son état actuel.
        /// </summary>
        /// <param name="parameter">Données utilisées par la commande.
        /// Si la commande n’exige pas que des données soient passées, cet objet peut avoir la valeur <see langword="null" />.</param>
        /// <returns>
        /// <see langword="true" /> si cette commande peut être exécutée ; sinon <see langword="false" />.
        /// </returns>
        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        /// <summary>
        /// Définit la méthode à appeler lorsque la commande est invoquée. Lance l'évènement <see cref="WorkStart"/> si il y'a des subscribers
        /// </summary>
        /// <param name="parameter">Données utilisées par la commande.
        /// Si la commande n’exige pas que des données soient passées, cet objet peut avoir la valeur <see langword="null" />.</param>
        void ICommand.Execute(object parameter)
        {
            WorkStart?.Invoke();
            ExecuteAsync().FireAndForgetSafeAsync(WorkError);
        }
        #endregion

        //public async Task Execute(object parameter)
        //{
        //    await Task.Delay(500);
        //}

        //public void Test()
        //{

        //}

    }
}
