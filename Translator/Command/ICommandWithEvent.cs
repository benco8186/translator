﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Translator.Command
{
    /// <summary>
    /// Ajoute des événements a l'interface ICommand
    /// </summary>
    public interface ICommandWithEvent : ICommand
    {
        event Action WorkStart;
        event Action WorkEnd;
        event Action<string> WorkError;
    }
}
