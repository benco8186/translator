﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Data;

namespace Translator.Model
{
    public class CheckItem : NotifyPropertyChangeBase
    {
        private LanguageValue _languageValue;
        public LanguageValue LanguageValue { get => _languageValue; 
            set 
            { 
                if(_languageValue != value)
                {
                    _languageValue = value;
                    NotifyPropertyChanged();
                }
            } 
        }

        private bool _isSelected;

        public bool IsSelected { get => _isSelected;
            set 
            { 
                if(_isSelected != value)
                {
                    _isSelected = value;
                    NotifyPropertyChanged();
                }
            } 
        }
        private bool _isEnabled = true;
        public bool IsEnabled
        {
            get => _isEnabled;
            set
            {
                if (_isEnabled != value)
                {
                    _isEnabled = value;
                    NotifyPropertyChanged();
                }
            }
        } 


    }
}
