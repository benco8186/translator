﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Translator.Model
{
    public class LanguageValue
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
