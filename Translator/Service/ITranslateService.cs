﻿using Google.Cloud.Translation.V2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Translator.Model;

namespace Translator.Service
{
    public interface ITranslateService
    {
         Task<string> GetTraductionAsync(string txt, IEnumerable<LanguageValue> languageValues, LanguageValue source);
    }
}
