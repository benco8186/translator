﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Translator.Model;
using Google.Api.Gax.ResourceNames;
using Google.Cloud.Translate.V3;
using Google.Cloud.Translation.V2;
using Google.Apis.Auth.OAuth2;
using System.Linq;

namespace Translator.Service
{
    public class TranslateService : ITranslateService
    {
        private readonly string ApiKey = "AIzaSyAgSV-sGOdAfjCW0aAIm-3hyJsJUe8ZRrQ";
        private readonly string Endpoint = "https://translation.googleapis.com/language/translate/v2";
        private readonly HttpClient client = new HttpClient();
        private readonly string TradName = "TRADNAME";

        public async Task<string> GetTraductionAsync(string txt, IEnumerable<LanguageValue> languageValues, LanguageValue source)
        {
            var isEnglishSource = source.Code == LanguageCodes.English;
            var output = new StringBuilder($"Translation = new Translation\n{{\n    {source.Name} = \"{txt}\",\n");
            foreach (var language in languageValues)
            {
                if(language.Code != source.Code)
                {
                    var request = await client.GetAsync(CreateUrl(source.Code, language.Code, txt));
                    var response = await request.Content.ReadAsStringAsync();
                    var googleData = JsonConvert.DeserializeObject<GoogleData>(response);
                    var trad = googleData.Data.Translations.FirstOrDefault()?.TranslatedText;
                    output.Append($"    {language.Name} = \"{trad}\",\n");
                }
                //else
                //{
                //    output.Append($"    {language.Name} = \"{txt}\",\n");
                //}
                
            }
            output.Append("}");
            return output.ToString();



        }

        private string CreateUrl(string source, string target, string txt)
        {
            return $"{Endpoint}?key={ApiKey}&source={source}&target={target}&q={txt}";
        }

        public class GoogleData
        {
            public Data Data { get; set; }
        }

        public class Data
        {
            public IEnumerable<TranslatedValue> Translations { get; set; }
        }

        public class TranslatedValue
        {
            public string TranslatedText { get; set; }
        }

    }
}
