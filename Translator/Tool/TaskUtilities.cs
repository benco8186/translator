﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Translator.Tool
{
    /// <summary>
    /// <c>TaskUtilities</c>
    /// Classe statique qui contient des méthodes d'extensions pour la classe <see cref="Task"/>
    /// </summary>
    public static class TaskUtilities
    {
        /// <summary>
        /// Permet d'effectuer une tache asynchrone en s'assurant que les exceptions sont traités
        /// </summary>
        /// <param name="task"></param>
        /// <param name="handler"></param>
        /// <param name="workErrorEvent">Evènement d'erreur lancé dans le catch</param>
        /// <returns></returns>
#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        public static async Task FireAndForgetSafeAsync(this Task task, Action<string> workErrorEvent = null)
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
        {
            try
            {
                await task;
            }
            catch (System.Exception ex)
            {
                workErrorEvent?.Invoke(ex.Message);
            }
        }
    }
}
