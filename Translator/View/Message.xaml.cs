﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Translator.View
{
    /// <summary>
    /// Logique d'interaction pour Message.xaml
    /// </summary>
    public partial class Message : Window
    {
        public Message()
        {
            InitializeComponent();
        }

        void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Change le titre de la fenetre
        /// </summary>
        /// <param name="title">The title.</param>
        public void ChangeTitle(string title)
        {
            this.Title = title;
        }

        void copyButton_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(messageTxtBlock.Text);
        }

        /// <summary>
        /// Formate le message en fonction de son statut
        /// </summary>
        /// <param name="result">The result.</param>
        public void DefineSuccessOrFail(OperationResult result)
        {
            switch (result)
            {
                case OperationResult.ERROR:
                    messageTxtBlock.Foreground = Brushes.Red;
                    break;
                case OperationResult.SUCCESS:
                    messageTxtBlock.Foreground = Brushes.Green;
                    break;
                case OperationResult.WARNING:
                    messageTxtBlock.Foreground = Brushes.Orange;
                    break;
            }
        }

        /// <summary>
        /// Enum qui permet de savoir le statut du message a afficher (erreur, succés...)
        /// </summary>
        public enum OperationResult
        {
            SUCCESS,
            ERROR,
            WARNING
        }
    }
}
