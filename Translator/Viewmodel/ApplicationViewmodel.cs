﻿using Google.Cloud.Translation.V2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation.Peers;
using System.Windows.Input;
using Translator.Command;
using Translator.Model;
using Translator.Service;
using Translator.View;
using static Translator.View.Message;

namespace Translator.Viewmodel
{
    public class ApplicationViewmodel : NotifyPropertyChangeBase
    {
        private readonly ITranslateService TranslateService;
        private string _text;
        public string Text {
            get => _text;
            set
            {
                if (value != _text)
                {
                    _text = value;
                    ClickTranslateCommand.RaiseCanExecuteChanged();
                    NotifyPropertyChanged();
                }
            }
        }
        private bool _isSpinnerActive;
        public bool IsSpinnerActive {
            get => _isSpinnerActive;
            set 
            {
                if(value != _isSpinnerActive)
                {
                    _isSpinnerActive = value;
                     NotifyPropertyChanged(); 
                }
            } 
        }
        public bool IsEnglishSource { get => ItemSelected.LanguageValue.Code == LanguageCodes.English; }
        public List<CheckItem> CheckItems { get; }
        private CheckItem _itemSelected;
        public CheckItem ItemSelected { get => _itemSelected; set
            {
                if (value != _itemSelected)
                {
                    _itemSelected = value;
                    ComboBox_SelectionChanged();
                    NotifyPropertyChanged();
                }
            }
        }
        public bool CanExecuteTranslate()
        {
           
             return !string.IsNullOrWhiteSpace(Text);

        }
        private AsyncCommand _clickTranslateButton;
        public AsyncCommand ClickTranslateCommand
        {
            get
            {
                return _clickTranslateButton ??= new AsyncCommand(() => Translate(), () => CanExecuteTranslate());
            }
        }

        
        public ApplicationViewmodel(ITranslateService translateService)
        {
            var englishItem = new CheckItem { IsSelected = true, LanguageValue = new LanguageValue { Name = "English", Code = LanguageCodes.English } };
            CheckItems = new List<CheckItem>
            {
                englishItem,
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "French", Code = LanguageCodes.French} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "Spanish", Code = LanguageCodes.Spanish} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "Russian", Code = LanguageCodes.Russian} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "Portuguese", Code = LanguageCodes.Portuguese} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "German", Code = LanguageCodes.German} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "Arabic", Code = LanguageCodes.Arabic} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "Chinese", Code = LanguageCodes.ChineseSimplified} },
                new CheckItem { IsSelected = true, LanguageValue = new LanguageValue{Name = "Japanese", Code = LanguageCodes.Japanese} },
            };
            ItemSelected = englishItem;
            TranslateService = translateService;
            ClickTranslateCommand.WorkError += (obj) => { IsSpinnerActive = false;  ShowMessage( obj, OperationResult.ERROR); };

        }
        public async Task Translate()
        {
            IsSpinnerActive = true;
            var trad = await TranslateService.GetTraductionAsync(Text, CheckItems.Where(x => x.IsSelected && x != ItemSelected).Select(x => x.LanguageValue), ItemSelected.LanguageValue);
            IsSpinnerActive = false;

            ShowMessage(trad);
            return;
            //Console.WriteLine(CheckItems.First().IsSelected);
            //Console.WriteLine("Test 12 12");
        }

        public void ShowMessage(string txt, OperationResult OperationResult = OperationResult.SUCCESS)
        {
            var message = new Message();

            message.DefineSuccessOrFail(OperationResult);
            message.messageTxtBlock.Text = txt;
            message.ShowDialog();
        }


        public void ComboBox_SelectionChanged()
        {
            var item = CheckItems.First(x => x.LanguageValue.Code == LanguageCodes.English);

            if (!IsEnglishSource)
            {
                item.IsSelected = true;
                item.IsEnabled = false;
            }
            else
            {
                item.IsEnabled = true;
            }
        }
    }
}
