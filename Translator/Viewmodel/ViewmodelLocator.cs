﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Translator.Viewmodel
{
    public class ViewmodelLocator
    {
        public ApplicationViewmodel ApplicationViewmodel => App.ServiceProvider.GetRequiredService<ApplicationViewmodel>();
    }
}
